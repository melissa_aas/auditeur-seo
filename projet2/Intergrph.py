import tkinter as tk
from tkinter import messagebox
from urllib.parse import urlparse
import Analyseur
import requests
from bs4 import BeautifulSoup


class Intergrph(tk.Tk):
    def __init__(self):
        super().__init__()
        self.analyseur_seo = Analyseur()

        self.title("Analyseur SEO")
        self.geometry("500x300")

        self.label_url = tk.Label(self, text="Saisir l'URL :")
        self.entree_url = tk.Entry(self, width=50)
        self.label_mots_cles = tk.Label(self, text="Saisir les mots clés (séparés par des virgules) :")
        self.entree_mots_cles = tk.Entry(self, width=50)
        self.bouton_analyser = tk.Button(self, text="Analyser", command=self.analyser)
        self.bouton_suivant = tk.Button(self, text="Suivant", command=self.afficher_resultats)

        self.label_url.pack()
        self.entree_url.pack()
        self.label_mots_cles.pack()
        self.entree_mots_cles.pack()
        self.bouton_analyser.pack()
        self.bouton_suivant.pack()

        self.resultats_url = []

    def analyser(self):
        url = self.entree_url.get()
        mots_cles = self.entree_mots_cles.get().split(',')

        if not url or not mots_cles:
            messagebox.showwarning("Attention", "Veuillez saisir à la fois l'URL et les mots clés.")
            return
        
        file ="C:\\Users\\azebm\\auditeur-seo\\projet2\\parasite.csv"
        self.analyseur_seo.charger_mots_parasites(file)
        mots_pertinents = self.analyseur_seo.analyser_url(url)

        domaine = self.analyseur_seo.extraire_nom_domaine(url)
        liens_internes, liens_externes = self.analyseur_seo.filtrer_urls_par_domaine(domaine, self.obtenir_liens_de_url(url))

        pourcentage_alt = self.calculer_pourcentage_alt(url)

        self.resultats_url.append({
            'url': url,
            'liens_internes': len(liens_internes),
            'liens_externes': len(liens_externes),
            'pourcentage_alt': pourcentage_alt,
            'mots_pertinents': mots_pertinents,
            'correspondance_mots_cles': any(mot.lower() in [mot[0] for mot in mots_pertinents] for mot in mots_cles)
        })

    def afficher_resultats(self):
        fenetre_resultats = tk.Toplevel(self)
        fenetre_resultats.title("Résultats de l'analyse SEO")

        for resultat in self.resultats_url:
            cadre_resultat = tk.Frame(fenetre_resultats, bd=2, relief=tk.GROOVE)
            cadre_resultat.pack(padx=10, pady=10, fill=tk.X)

            tk.Label(cadre_resultat, text=f"URL : {resultat['url']}").pack(anchor=tk.W)
            tk.Label(cadre_resultat, text=f"Liens internes : {resultat['liens_internes']}").pack(anchor=tk.W)
            tk.Label(cadre_resultat, text=f"Liens externes : {resultat['liens_externes']}").pack(anchor=tk.W)
            tk.Label(cadre_resultat, text=f"Pourcentage Alt : {resultat['pourcentage_alt']}%").pack(anchor=tk.W)
            tk.Label(cadre_resultat, text="Mots Pertinents :").pack(anchor=tk.W)
            for i, mot in enumerate(resultat['mots_pertinents']):
                tk.Label(cadre_resultat, text=f"{i + 1}. {mot[0]} : {mot[1]} occurrences").pack(anchor=tk.W)
            tk.Label(cadre_resultat, text=f"Correspondance Mots Clés : {'Oui' if resultat['correspondance_mots_cles'] else 'Non'}").pack(anchor=tk.W)

    def obtenir_liens_de_url(self, url):
    
        try:
            response = requests.get(url)
            response.raise_for_status()
            soup = BeautifulSoup(response.text, 'html.parser')
            # Utiliser soup pour extraire les liens de l'URL (par exemple, les balises <a>)
            links = [a['href'] for a in soup.find_all('a', href=True)]
            return links
        except requests.RequestException as e:
            raise Exception(f"Erreur lors de la récupération des liens depuis l'URL : {e}")


    def calculer_pourcentage_alt(self, url):
    
        try:
            response = requests.get(url)
            response.raise_for_status()
            soup = BeautifulSoup(response.text, 'html.parser')
            img_tags = soup.find_all('img')
            total_images = len(img_tags)
            alt_present_images = sum(1 for img in img_tags if img.get('alt'))
            alt_percentage = (alt_present_images / total_images) * 100 if total_images > 0 else 0
            return alt_percentage
        except requests.RequestException as e:
             raise Exception(f"Erreur lors du calcul du pourcentage Alt depuis l'URL : {e}")


if __name__ == "__main__":
    app = Intergrph()
    app.mainloop()
