from collections import Counter
import re
import requests
import csv
from bs4 import BeautifulSoup
from urllib.parse import urlparse #permet de décomposer une URL en ses composants comme le nom

# Etape 1 : Cration d'une fonction qui permet de filtrer les caractères 
# et le nombre d'occurence des mots présents dans un texte

 
def fonction_une(texte):
    txt = re.findall(r'\b\w+\b',texte.lower())
    nbr_occurences = Counter(txt)
    dictionnaire = sorted(nbr_occurences.items(),key = lambda x: x[1], reverse= True)
    return dictionnaire

teste1 = "On doit tester ce texte pour tester si notre teste fonctionne. Teste!"
result1 = fonction_une(teste1)
print("Le résultat de l'étape 1 : \n",result1)


# Etape 2 : Suppression des mots parasites contenus dans un texte 
# Fonction Lambda est utilisée pour vérifier si l'élément x[0]
# n'est pas dans la liste des mots parasites telsque le,la...
# Et la fonction Filter() permet de filtrer justement les éléments de données en ne gardant
# que ceux pour lesquels la condition est vraie


def fonction_deux(liste_mots_parasites,donnees):
    mots_parasites = filter(lambda x: x[0] not in liste_mots_parasites,donnees)
    return list(mots_parasites)

donnees_test =[("le",5),("la",2),("bonjour",1),("sac",6),("les",1)]
parasites = ["le","la","les"]
result2 = fonction_deux(parasites,donnees_test)
print("Résultat après la supp des parasites étape 2: ",result2)

# Etape 3 : Récupérer les mots parasites dans un CSV
def fonction_trois(path_file):
    liste_mots_parasites = []
    with open(path_file) as f : 
        lire_csv = csv.reader(f)
        for l in lire_csv:
            liste_mots_parasites.extend(l)
    return liste_mots_parasites

file ="C:\\Users\\azebm\\auditeur-seo\\projet1\\parasite.csv"
word = fonction_trois(file)
print("Le résultat de l'étape 3",word)

# Etape 4 : Teste des fonctions faites précédemment ! 
teste2 = "Ici on a le texte à analyser. Faudrait analyser tres bien le texte "
words_occu = fonction_une(teste2)
words_1 = fonction_trois(file)
words_2 = fonction_deux(words_1,words_occu)
print("Le résultat de l'étape 4 : ",words_2[:3])

# Etape 5 : Supp les balises HTML
# Créer un objet en passant par le texte HTML et html.parser qui

def fonction_cinq(html):
    h_tml = BeautifulSoup(html,'html.parser')
    textes = h_tml.get_text()
    return textes

teste3 = "<br> <p>une phrase texte avec balise</p></br>"
result3 = fonction_cinq(teste3) 
print("Le résultat de l'étape 5 :",result3)


# Etape 6 : Récup les valeurs associées à des balises :
def fonction_six(html,attribute,balise) :
    beauti_ful = BeautifulSoup(html,'html.parser')
    elt = beauti_ful.find_all(balise)
    value = []
    for e in elt :
        values = e.get(attribute)
        value.append(values)
    return value

teste4 = '''<p><Container>
               <Row className='my-3'>
                   <h1>✏️ Ajouter un mot</h1>
                   <h4>Entrez un mot utilisé dans les rébus</h4>
                </Row>
        
            </p></Container>''' 
             
result4 = fonction_six(teste4,'Row','classeName')
print("Le résultat de l'étape 6 :",result4)

# Etape 7 : Recup les attributs alt et href 


# Etape 8 : Extraction du nom de URL
def fonction_huit(url):
    u_rl = urlparse(url) #stocker les différentes parties de l'URL
    nom = u_rl.netloc
    return nom #retourner le nom du domaine extrait de URL

teste5 = "https://www.deepl.com/fr/translator"
result5 = fonction_huit(teste5)
print("Le résultat de l'étape 8 :",result5)

# Etape 9 : Toujours dans les URL
def fonction_neuf(nom,url_list):
    url_dom = []
    liens = []
    for u in url_list:
        if fonction_huit(l)==nom :
            url_dom.append(l)
        else :
            liens.append(l)
    return url_dom,liens

#teste6 = ["","",""]
#result6 =""


# Etape 10 : Récup texte HTML dans une URL
# Effectuer une requête GET à une URL pour recup texte HTML
# Si result =200 donc c'est reussi ça nous retourne le texte
# Sinon return 0
def fonction_dix(url):
    try :
        u_r_l = requests.get(url)
        u_r_l.raise_for_status() #lever d'exception for HTTP erreur
        return u_r_l.text
    except requests.RequestException as l :
        print("Erreur ! {l}")
        return None
    
teste7 = "https://www.deepl.com/fr/translator"
result7 =fonction_dix(teste7) 
print("Le résultat de l'étape 10 :",result7[:50]) #on limite le nbr de texte 

# Etape 11 : Le programme qui sert l'audit de notre page 
def prog_principal(lien):
    donne1 = fonction_dix(lien)
    donne2 = fonction_cinq(donne1)
    donne3 = fonction_une(donne2)
    donne4 = fonction_trois(file)
    donne5 = fonction_deux(donne4,donne3)
    
    
    
    
testefinal = 'https://www.google.com/maps'
resultifinal = prog_principal(testefinal)
print("Final result : ",resultifinal)