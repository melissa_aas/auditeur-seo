from collections import Counter
import re
import requests
from bs4 import BeautifulSoup
from urllib.parse import urlparse

class Analyseur:
    def __init__(self):
        self.mots_parasites = []

    def obtenir_occurrences_mots(self, texte):
        mots = re.findall(r'\b\w+\b', texte.lower())
        occurrences = Counter(mots)
        return occurrences

    def charger_mots_parasites(self, chemin_fichier):
        with open(chemin_fichier) as f:
            lecteur = [mot.lower() for ligne in f for mot in ligne.split(',')]
        self.mots_parasites = lecteur

    def filtrer_mots_parasites(self, donnees):
        return [(mot, compte) for mot, compte in donnees if mot not in self.mots_parasites]

    def nettoyer_html(self, html):
        soup = BeautifulSoup(html, 'html.parser')
        texte = soup.get_text()
        return texte

    def analyser_url(self,url):
        try:
            texte_html = self.obtenir_texte_html(url)
            texte_nettoye = self.nettoyer_html(texte_html)
            occurrences_mots = self.obtenir_occurrences_mots(texte_nettoye)
            mots_pertinents = self.filtrer_mots_parasites(occurrences_mots)[:3]
            return mots_pertinents
        except Exception as e:
            raise Exception(f"Erreur lors de l'analyse de l'URL : {e}")

    def obtenir_texte_html(self, url):
        try:
            reponse = requests.get(url)
            reponse.raise_for_status()
            return reponse.text
        except requests.RequestException as e:
            raise Exception(f"Erreur lors de la récupération de l'URL : {e}")

    def extraire_nom_domaine(self, url):
        parsed_url = urlparse(url)
        return parsed_url.netloc

    def filtrer_urls_par_domaine(self, domaine, liste_urls):
        urls_domaine = [url for url in liste_urls if self.extraire_nom_domaine(url) == domaine]
        urls_externes = [url for url in liste_urls if self.extraire_nom_domaine(url) != domaine]
        return urls_domaine, urls_externes
